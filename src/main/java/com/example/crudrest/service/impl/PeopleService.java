package com.example.crudrest.service.impl;


import com.example.crudrest.entity.Person;
import com.example.crudrest.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeopleService {

    private final PersonRepository personRepository;

    @Autowired
    public PeopleService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAll(){
        return personRepository.findAll();

    }

    public Person getById(int id){
        return personRepository.findById(id).get();
    }

    public Person createPerson(String name, int age){
        Person createPerson = new Person();
        createPerson.setName(name);
        createPerson.setAge(age);
        return personRepository.save(createPerson);
    }

    public Person updatePerson(int id, String name, int age){
        Person updatePerson = new Person(id, name, age);

        return personRepository.save(updatePerson);
    }

    public void deletePerson(int id){
        personRepository.deleteById(id);
    }
}
