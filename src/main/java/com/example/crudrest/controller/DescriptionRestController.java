package com.example.crudrest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/description")
public class DescriptionRestController {

    @GetMapping
    public String sayHello(){
        return "This is simply task manager app.";
    }

}
