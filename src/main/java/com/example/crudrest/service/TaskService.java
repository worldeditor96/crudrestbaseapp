package com.example.crudrest.service;

import com.example.crudrest.entity.Task;

import java.util.List;

public interface TaskService {
    Task createTask(String title, boolean isCompleted, String priority);

    Task updateTask(Long id, String title, boolean completed, String priority);

    Task getTaskById(Long id);

    void deleteTaskById(Long id);

    List<Task> findAll();
}
