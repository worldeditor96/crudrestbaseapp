package com.example.crudrest.service.impl;

import com.example.crudrest.entity.Task;
import com.example.crudrest.repository.TaskRepository;
import com.example.crudrest.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {
    private TaskRepository taskRepository;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task createTask(String title, boolean isCompleted, String priority) {
        Task task = mapTask(title, isCompleted, priority);
        return taskRepository.save(task);
    }

    @Override
    public Task updateTask(Long id, String title, boolean completed, String priority) {
        Task task = mapTask(title, completed, priority);
        task.setId(id);
        return taskRepository.save(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task getTaskById(Long id) {
        return taskRepository.findById(id).get();
    }

    @Override
    public void deleteTaskById(Long id) {
        taskRepository.deleteById(id);
    }

    //create
    private  Task mapTask(String title, boolean isCompleted, String priority) {
        Task task = new Task();
        task.setCompleted(isCompleted);
        task.setTitle(title);
        task.setCreationDate(LocalDate.now());
        task.setPriority(priority);
        return task;
    }
}
