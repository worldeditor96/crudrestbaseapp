package com.example.crudrest.controller;


import com.example.crudrest.entity.Person;
import com.example.crudrest.service.impl.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/people")
public class PeopleController {

    private final PeopleService peopleService;

    @Autowired
    public PeopleController(PeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @GetMapping
    public List<Person> getAllPerson (){
        return peopleService.getAll();
    }


    @PostMapping
    public Person cretePerson(@RequestBody Person createPerson){
        return peopleService.createPerson(createPerson.getName(), createPerson.getAge());
    }


    //////////////////////////////////////////////////////
    //??????
//    @PutMapping
//    public Person updatePerson(@RequestBody Person updatePerson){
//        return peopleService.updatePerson(updatePerson.getId(), updatePerson.getName(), updatePerson.getAge());
//    }

    //&&&&&
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Person> updatePer(@RequestBody Person updatePerson){
        Person personUp = peopleService.updatePerson(updatePerson.getId(), updatePerson.getName(), updatePerson.getAge());
        return ResponseEntity.ok(personUp);
    }
//////////////////////////////////////

    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable("id") int id){
        peopleService.deletePerson(id);
    }


}
